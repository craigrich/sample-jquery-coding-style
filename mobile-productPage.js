var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};

cuprinol.mobile.productPage = {

    init: function() {
        this._bindColorLinks();
        this._moveDesc();
    },

    _moveDesc: function() {
        $('.product-summary p').insertAfter(".grid_6").addClass('mobile__product__description');
    },

    _bindColorLinks: function() {
        var $colors = $('ul.colours li a, ul.colours li');
        $colors.unbind();
    }
};

//Use on load event if not using module pattern
$(function() {
    if (cuprinol.isMobile) cuprinol.mobile.productPage.init();
});