/**
 * Simulates the Attr on touch, prevents ghost touches on scrol
 * @param [event] e The list item clicked
 * Todo: Move to a utilites file
 */
$.fn.setActiveListItem = function() {
    this.on('touchend', function(e) {
        var self = this;
        $(this).addClass('active');
        setTimeout(function() {
            $(self).removeClass('active');
        }, 100);
        $(this).off('touchend');
    });
    this.on('touchmove', function(e) {
        $(this).off('touchend');
    });
    return this;
};