var cuprinol = cuprinol || {};
cuprinol.mobile = cuprinol.mobile || {};

cuprinol.mobile.subPage = {

    init: function() {
        console.log('fire');
        this.bindVideoLinks();
        this.bindProductLinks();
        this.bindColorLinks();
        this.bindColorSelector();
        this.bindCarousel();
    },

    bindActions: function() {},

    /**
     * Takes the Iframe video source and appends it to native link
     * Takes advantage of using native youtube/video players in mobiles by using native link
     */
    bindVideoLinks: function() {
        try {
            var $el = $('.button.open-overlay'),
                videoLink = $el ? $($('.button.open-overlay').data().content)[0].src : '';
            if ($el) $el.unbind().attr('href', '' + videoLink + '');
        } catch (e) {}
    },

    /**
     * Take Link from each product's button and append it a product <li> href.
     */
    bindProductLinks: function() {
        var $products = $('.product-listing .grid_3'),
            productLink;

        if ($products) {
            $products.each(function(index, el) {
                productLink = $(this).find('.product-details .left .buttons a:first-of-type').attr('href');
                $(this)
                    .children('a')
                    .unbind()
                    .attr('href', '' + productLink + '');
            });
        }
    },

    /**
     * Add Link to Color Picker button that directs to Color picker page.
     */
    bindColorLinks: function() {
        var $el = $(".sheds-colour-selector a[href$='#products']");
        if ($el) $el.unbind().attr('href', '/products');
    },


    bindColorSelector: function() {
        var $el = $('.colour-selector-copy');
        if ($el) {
            $el.children('h4').text('Colours');
            $el.children('p').text('Click below to see some ideas to brighten up wooden furniture around your garden.');
            $el.children('a').text('Try our colour selector').attr('href', '/garden_colour/colour_selector/index.jsp');
        }

    },

    /**
     * Initialise Primary Carousel on all subpages
     * @return {[type]} [description]
     */
    bindCarousel: function() {
        var $el = $('#js-carousel-primary');

        if ($el) {
            $el.swiper({
                mode: 'horizontal',
                loop: true,
                pagination: '#js-subpage-pagination',
                createPagination: true,
                paginationAsRange: true
            });
        }
    }
};


//Use on load event if not using module pattern
$(function() {
    if (cuprinol.isMobile) cuprinol.mobile.subPage.init();
});